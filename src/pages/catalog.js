import React from 'react'
import styled from 'styled-components'
import { Layout } from '../components/Layout'
import { BorderWrapper } from '../components/BorderWrapper'
import { Container } from '../components/Container'
import { CatalogVideo } from '../components/CatalogVideo'
import { GridItem } from '../components/GridItem'
import { theme } from '../theme'

const Grid = styled(Container).attrs({ maxWidth: 72, mt: [3, 4] })`
  display: grid;
  grid-gap: ${theme.space[3]}px;
  ${theme.mediaQueries.sm} {
    grid-template-columns: repeat(auto-fill, minmax(20rem, 1fr));
    grid-gap: ${theme.space[4]}px;
  }
`

const Video = ({ color, ...props }) => (
  <GridItem style={{ display: 'flex', flexDirection: 'column' }}>
    <img src="https://source.unsplash.com/random/704x704" />
    <CatalogVideo color={color} {...props} />
  </GridItem>
)

export default () => (
  <Layout>
    <BorderWrapper>
      <Grid>
        <Video color={theme.colors.blue} />
        <Video color={theme.colors.pink} />
        <Video color={theme.colors.yellow} />
      </Grid>
    </BorderWrapper>
  </Layout>
)
