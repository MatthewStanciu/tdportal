import React from 'react'
import styled from 'styled-components'
import { theme } from '../theme'
import { Background } from '../components/Background'
import { Layout } from '../components/Layout'
import { Logo } from '../components/Logo'
import { Action } from '../components/Action'
import { Field } from '../components/Field'
import { Box, Card, Heading, Button } from 'rebass'

const Form = styled(Box).attrs({ as: 'form', width: 1 })`
  display: block;
`

const SignInCard = styled(Card).attrs({
  bg: 'white',
  width: 1,
  mx: 'auto',
  py: [4, 5],
  px: [3, 4]
})`
  max-width: 24rem;
  height: 32rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-shadow: 0 8px 32px rgba(0, 0, 0, 0.125);
  border-radius: ${theme.radii[2]};
  ${Heading} {
    line-height: 1.25;
  }
`

const SignInBox = props => (
  <SignInCard>
    <Logo src="https://cdn.glitch.com/2ed3d039-13f8-4cad-ac92-80eb5a803ccd%2Flogo.png?1550797006059" />
    <Heading as="h1" textAlign="center" fontSize={5} color="pink" my={[2, 3]}>
      Sign in to TinyDocs
    </Heading>
    {/* TODO: add form action */}
    <Form>
      <Field
        type="email"
        name="email"
        label="Your email"
        placeholder="you@tinydocs.co"
      />
      <Field
        type="password"
        name="password"
        label="Password"
        placeholder="8+ characters"
      />
    </Form>
    <Action type="submit" bg="pink" mt={[2, 3]}>
      Submit
    </Action>
  </SignInCard>
)

export default () => (
  <Layout>
    <Background center>
      <SignInBox />
    </Background>
  </Layout>
)
