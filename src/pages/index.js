import React from 'react'
import styled from 'styled-components'
import { Card, Image, Heading, Text } from 'rebass'
import { Action } from '../components/Action'
import { BorderWrapper } from '../components/BorderWrapper'
import { Container } from '../components/Container'
import { Video, Article } from '../components/GridItem'
import { Layout } from '../components/Layout'
import { Logo } from '../components/Logo'
import { theme } from '../theme'

const Head = styled(Heading).attrs({
  as: 'h1',
  fontSize: [5, 6, 7],
  color: 'pink'
})`
  line-height: 1.125;
`

const Subhead = styled(Heading).attrs({ color: 'blue' })`
  font-size: 1.5rem;
`

const Grid = styled(Container).attrs({ maxWidth: 72, mt: [3, 4] })`
  display: grid;
  grid-gap: ${theme.space[3]}px;
  ${theme.mediaQueries.sm} {
    grid-template-columns: repeat(auto-fill, minmax(20rem, 1fr));
    grid-gap: ${theme.space[4]}px;
  }
`

const Header = styled(Container).attrs({ maxWidth: 64, px: 3, mb: [5, 6] })`
  display: grid;
  grid-gap: ${theme.space[3]}px;
  ${theme.mediaQueries.md} {
    align-items: flex-end;
    grid-template-columns: 3fr 2fr;
    grid-gap: ${theme.space[4]}px;
  }
`

const Yellow = styled(Card).attrs({ bg: 'yellow', width: 1 })`
  height: 32rem;
  box-shadow: 0 8px 32px rgba(0, 0, 0, 0.125);
  border-radius: ${theme.radii[2]};
`

export default () => (
  <Layout>
    <BorderWrapper>
      <Header>
        <Container maxWidth={36} mx={0} pt={[4, 5]} pb={[3, 4]}>
          <Logo src="https://cdn.glitch.com/2ed3d039-13f8-4cad-ac92-80eb5a803ccd%2Flogo.png?1550797006059" />
          <Head mt={3}>A kid’s companion to better health</Head>
          <Action mt={3} mb={5}>
            Watch our videos
          </Action>
          <Subhead>Our mission</Subhead>
          <Text style={{ maxWidth: '32rem' }}>
            TinyDocs offers a library of fun and educational cartoons, covering
            topics such as strep throat, tonscillectomies, and asthma that
            explain the health challenge to kids and families.
          </Text>
        </Container>
        <Yellow />
      </Header>
      <Grid my={[3, 4, 5]} px={3}>
        <Video />
        <Video />
        <Video />
      </Grid>
      <Grid my={[3, 4]} px={3}>
        <Article />
        <Article />
        <Article />
      </Grid>
    </BorderWrapper>
  </Layout>
)
