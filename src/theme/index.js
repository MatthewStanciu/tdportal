import React from 'react'
import { createGlobalStyle, ThemeProvider as Root } from 'styled-components'
import config from './config'

export const theme = config

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    font-weight: inherit;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -webkit-appearance: none;
    -moz-appearance: none;
  }
  html,
  body {
    min-height: 100%;
    min-width: 100%;
  }
  body {
    padding: 0;
    margin: 0;
    position: relative;
    font-size: ${theme.fontSizes[3]}px;
    font-family: ${theme.font};
    font-weight: normal;
    line-height: 1.5;
    border-top: 6px solid ${theme.colors.pink};
    color: ${theme.colors.black};
  }
  h1,
  h2,
  h3 {
    font-weight: 900;
  }
  h1 + h2,
  p {
    font-weight: normal !important;
  }
  a {
    box-shadow: none;
    text-decoration: none;
  }
  ul, ol {
    margin: 0;
    padding: 0;
  }
  p > a,
  strong,
  b {
    font-weight: bold !important;
  }
`

const ThemeProvider = ({ children }) => (
  <Root theme={config}>
    <>
      <GlobalStyle />
      {children}
    </>
  </Root>
)

export default ThemeProvider
