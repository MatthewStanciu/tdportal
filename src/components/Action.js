import styled from 'styled-components'
import { Button } from 'rebass'
import { theme } from '../theme'

export const Action = styled(Button)`
  border-radius: 0 ${theme.radius} ${theme.radius} ${theme.radius};
  font-family: ${theme.font};
  text-transform: uppercase;
`
