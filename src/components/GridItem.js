import React from 'react'
import styled from 'styled-components'
import { Card, Text } from 'rebass'
import { theme } from '../theme'

export const GridItem = styled(Card).attrs({ bg: 'white' })`
  overflow: hidden;
  min-height: 12rem;
  border-radius: ${theme.radii[2]};
  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.125);
  line-height: 0;
  transition: ${theme.transition} all 0.125s;
  &:hover,
  &:focus {
    box-shadow: 0 8px 32px rgba(0, 0, 0, 0.125);
    transform: translateY(-3px);
  }
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`

export const GridItemBanner = styled(Text).attrs({
  fontWeight: 'bold',
  fontSize: 3,
  color: 'slate',
  bg: 'blueLight',
  textAlign: 'center',
  p: 3
})`
  display: block;
  text-transform: uppercase;
  opacity: 50%;
  line-height: 1;
`

export const Video = () => (
  <GridItem>
    <img src="https://source.unsplash.com/random/704x704" />
  </GridItem>
)

export const Article = () => (
  <GridItem>
    <img src="https://source.unsplash.com/random/706x704" />
    <GridItemBanner>Read more</GridItemBanner>
  </GridItem>
)
