import styled from 'styled-components'
import { Flex } from 'rebass'
import { theme } from '../theme'

export const BorderWrapper = styled(Flex).attrs({
  as: 'main',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  bg: 'white',
  p: 3,
  m: 3
})`
  position: relative;
  border-radius: ${theme.radius};
  box-shadow: 0 15px 30px 0 rgba(0, 0, 0, 0.11),
    0 5px 15px 0 rgba(0, 0, 0, 0.08);
  height: 100%;
  // transition: 0.2s ${theme.transition};
  // &:hover {
  //   transform: translateY(-3px);
  // }
`
