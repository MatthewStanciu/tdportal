import styled from 'styled-components'
import { theme } from '../theme'
import { Image } from 'rebass'

export const Logo = styled(Image).attrs({
  bg: 'white',
  width: [48, 64],
  borderRadius: '50%'
})`
  display: block;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.125);
`
