import React from 'react'
import { Flex } from 'rebass'
import { colors } from '../theme/config'

export const Subhead = styled(Flex).attrs({
  align: 'center',
  justify: 'center',
  color: colors.blue
})`
  text-align: center;
  letter-spacing: 0.1em;
`
