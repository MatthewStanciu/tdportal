import React from 'react'
import styled from 'styled-components'
import { Flex } from 'rebass'

export const Intro = styled(Flex).attrs({
  flexDirection: 'row',
  alignItems: 'center'
})
