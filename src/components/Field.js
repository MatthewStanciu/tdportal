import React from 'react'
import styled from 'styled-components'
import { Text, Flex } from 'rebass'
import { Input } from './Input'
import PropTypes from 'prop-types'
import { theme } from '../theme'

export const Label = styled(Text).attrs({
  as: 'label',
  color: 'black',
  fontSize: 2,
  width: 1
})`
  display: block;
  font-weight: 500;
  a {
    color: ${theme.colors.blue};
    text-decoration: underline;
  }
`

export const Field = ({ type, name, label, placeholder, mb = 2, ...props }) => (
  <Label className={type} id={name} mb={mb}>
    {label}
    <Input name={name} type={type} placeholder={placeholder} {...props} />
  </Label>
)

Field.propTypes = {
  /** choose alternate field type (like email, password, etc) */
  type: PropTypes.oneOf([
    'date',
    'email',
    'file',
    'number',
    'password',
    'tel',
    'url',
    'text'
  ]),
  name: PropTypes.string.isRequired,
  /** label text */
  label: PropTypes.string.isRequired,
  /** placeholder text */
  placeholder: PropTypes.string
}

Field.defaultProps = {
  type: 'text',
  mb: 2
}

export default Field
