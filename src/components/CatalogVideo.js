import React from 'react'
import styled from 'styled-components'
import { Card, Text } from 'rebass'
import { theme } from '../theme'

/*
const VideoCard = styled(Card).withComponent('img').attrs({
    bg: 'white',
    width: 1,
    mx: 'auto',
    py: [4, 5],
    px: [3, 4]
})`
    max-width: 18rem;
    height: 12rem;
    box-shadow: 0 8px 32px rgba(0, 0, 0, 0.125);
    border-radius: ${theme.radii[2]};
    overflow: hidden;
    img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
`*/

const VideoInfoCard = styled(Card).attrs({
  width: 1,
  mx: 0,
  p: [2, 3]
})`
  opacity: 50%;
  display: flex;
  flex-direction: column;
  line-height: 0.5;
  color: ${theme.colors.white};
  border-radius: 0 0 ${theme.radii[2]} ${theme.radii[2]};
`

// i'll fix this later
export const CatalogVideo = ({ color, ...props }) => (
  <VideoInfoCard bg={color} {...props}>
    <Text fontSize={theme.fontSizes[1]}>FIRST AID</Text>
    <p>Band aids</p>
    <Text fontSize={theme.fontSizes[0]}>Video description</Text>
  </VideoInfoCard>
)

/*export const Video = (category, title, description, {...props}) => (
    <VideoCard>
        <img src="https://source.unsplash.com/random/706x704" />
        <VideoInfo />
    </VideoCard>
)*/
