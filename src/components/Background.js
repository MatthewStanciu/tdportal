import styled, { css } from 'styled-components'
import { theme } from '../theme'
import { Box } from 'rebass'
// import { Container } from './Container'

export const Background = styled(Box).attrs({
  as: 'main',
  bg: 'yellow',
  p: 3
})`
  min-height: calc(100vh - 6px); // account for page top border
  ${props =>
    props.center &&
    css`
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
    `}
`
