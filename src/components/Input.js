import React from 'react'
import styled from 'styled-components'
import { Box } from 'rebass'
// import { fontSize, space, width, color } from 'styled-system'
import { theme } from '../theme'

const chevron = () => {
  const props = `xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'`
  const slate = '%23' + theme.colors.slate.replace('#', '')
  const pathProps = `fill='${slate}' d='M2 0L0 2h4zm0 5L0 3h4z'`
  return `%3Csvg ${props}%3E%3Cpath ${pathProps}/%3E%3C/svg%3E`
}

export const Input = styled(Box).attrs({
  as: 'input',
  width: 1,
  m: 0,
  py: 1,
  px: 2,
  fontSize: 2,
  color: 'inherit',
  bg: 'transparent'
})`
  appearance: none;
  display: block;
  vertical-align: middle;
  max-width: 32rem;
  min-height: 36px;
  line-height: inherit;
  font-family: inherit;
  font-weight: 400;
  background-color: transparent;
  border-radius: ${theme.radius};
  border-width: 1px;
  border-style: solid;
  border-color: ${theme.colors.smoke};
  transition: ${theme.transition} border-color;
  ::placeholder {
    color: ${theme.colors.muted};
  }
  ::-ms-clear {
    display: none;
  }
  &:focus {
    outline: none;
    border-color: ${theme.colors.blue};
  }
  &[type='select'] {
    background: #fff url("data:image/svg+xml;charset=utf8,${chevron()}") no-repeat right 0.75rem center;
    background-size: 0.5rem;
  }
  &[type='textarea'] {
    resize: vertical;
  }
`
// ${fontSize} ${space} ${width} ${color};
// `

Input.displayName = 'Input'

// Input.defaultProps =

// export const InputSelect = Input.withComponent('select')
// export const InputTextarea = Input.withComponent('textarea')

// export default Input
