import React from 'react'

export default class Form extends React.Component {
  render() {
    return (
      <form>
        <input type="text" placeholder={this.props.type} />
      </form>
    )
  }
}
